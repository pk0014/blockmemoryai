import os 
import random
import numpy as np
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.dataloader import default_collate


class TextDataLoader(DataLoader):
    def __init__(self, textDataset, batch_size=1, shuffle=False, sampler=None,
                 batch_sampler=None, num_workers=0, collate_fn=default_collate,
                 pin_memory=False, drop_last=False, timeout=0,
                 worker_init_fn=None):
        super().__init__(textDataset, batch_size, shuffle, sampler, batch_sampler, num_workers, collate_fn,
                         pin_memory, drop_last, timeout, worker_init_fn)

        self.counter = 0

    
    def oneHotToText(self, onehot):
        return self.dataset.oneHotToText(onehot)
        
    def textToOneHot(self, text):
        return self.dataset.textToOneHot(text)

    def getInputShape(self):
        return self.dataset.preTextLength * len(self.dataset.corpus)

    def getCorpusSize(self):
        return len(self.dataset.corpus)

    def increaseCounter(self, i=1):
        self.counter += i

    def getCorpus(self):
        return self.dataset.corpus

class TextDataset(Dataset):
    def __init__(self, preTextLength, postTextLength, train = True):
        self.corpus = []
        self.reviews = []
        self.preTextLength = preTextLength
        self.postTextLength = postTextLength
        self.train = train
        self.split = 0.6

        self.loadFileToMemory()

    def __len__(self):
        l = 0
        for r in self.reviews:
            l += len(r) - (self.preTextLength + self.postTextLength)
        return l

    def __getitem__(self, idx):
        pre, post = self.getRandomText(self.preTextLength, self.postTextLength, idx)
        return pre, post
        
    





    def getRandomText(self, preLength, postLength, seed=None):
        random.seed(seed)

        #TODO: Optimise this
        review = random.choice(self.reviews)
        while not len(review) > preLength+postLength:
            review = random.choice(self.reviews)

        startingPoint = random.randint(0, len(review)-(preLength+postLength))
        preText = review[startingPoint:startingPoint + preLength]
        postText = review[startingPoint + preLength: startingPoint + preLength + postLength]
        return preText, postText


    

    def oneHotToText(self, oneHot):
        text = ""
        number_of_letters = len(oneHot)/len(self.corpus)
        if number_of_letters % 1 != 0:
            raise Exception('One hot array ({}) cannot be devided by corpus size: {}'.format(len(oneHot)))
        for i in range(0, int(number_of_letters)):
            letter_idx = np.argmax(oneHot[i*len(self.corpus):(i*len(self.corpus))+len(self.corpus)])
            text += self.corpus[letter_idx]

        return text




    def textToIdx(self, text, dtype=np.int64):
        a = np.zeros((len(text)), dtype=dtype)
        for idx, l in enumerate(text):
            a[idx] = self.corpus.index(l)
        return a

    def idxToText(self, idxs):
        text = ""
        for idx in idxs:
            text += self.corpus[idx]
        return text




    def loadFileToMemory(self, path="dataset/reviews.txt"):

        if(os.path.exists(path) == False):
            raise Exception('File not found : {}'.format(path))

        
        with open(path, 'r') as f:
            reviews_whole = f.read()    
            reviews_whole = reviews_whole.replace("  br  ", "")
            reviews_whole = reviews_whole.replace("  ", " ")
        reviews_temp = reviews_whole.split('\n')
        
        if self.train:
            reviews_temp = reviews_temp[:int(len(reviews_temp)*self.split)]
        else:
            reviews_temp = reviews_temp[int(len(reviews_temp)*self.split):]

        for r in reviews_temp:
            if len(r) >= self.preTextLength + self.postTextLength:
                self.reviews.append(r)

        self.corpus = set(reviews_whole)
        self.corpus = list(self.corpus)
        self.corpus.remove("\n")

    

    
if __name__ == '__main__':
    pass
    """dataset = TextDataset(50, 10)
    dataloader = TextDataLoader(dataset, batch_size=10, shuffle=True)
    output_pre, output_post = next(iter(dataloader))
    print(output_pre, output_post)
    output_pre = dataset.textToOneHot(output_pre[0])
    output_pre = dataset.oneHotToText(output_pre)
    print(output_pre)"""
    