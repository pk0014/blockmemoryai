from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets, transforms
from TextDataset import TextDataLoader, TextDataset
import numpy as np
import random
from DynamicLinearLayer import DynamicLinearLayer
import Utils

class Net(nn.Module):
    def __init__(self, preTextLength, corpus):
        super(Net, self).__init__()
        self.corpus = list(corpus)


        self.lin1 = nn.Linear(preTextLength*len(corpus), 100)
        self.relu1 = nn.ReLU()
        self.lin2 = nn.Linear(100, 512)
        self.relu2 = nn.ReLU()
        self.lin3 = DynamicLinearLayer(512, len(corpus))

    def forward(self, x):
        x = x.view(x.size(0), -1)
        x = self.lin1(x)
        x = self.relu1(x)
        x = self.lin2(x)
        x = self.relu2(x)
        x = self.lin3(x)
        return x


    def expand_output(self, output_features):
        with torch.no_grad():
            self.lin3.expand_output_features(output_features)
    
    def add_output(self, text):
        print("Adding output", text)
        self.corpus.append(text)
        self.expand_output(len(self.corpus))

    def targetToIdx(self, target):
        arr = np.zeros((len(target)), dtype=np.int64)
        for textidx, text in enumerate(target):
            foundIdx = -1
            foundLetter = ""
            for idx, l in enumerate(self.corpus):
                if text.startswith(l) and len(l) > len(foundLetter):
                    foundIdx = idx
                    foundLetter = l
            arr[textidx] = foundIdx

        return arr

    def idxToText(self, idx):
        return self.corpus[idx]


    def oneHotToText(self, oneHot):
        text = ""
        number_of_letters = len(oneHot)/len(self.corpus)
        if number_of_letters % 1 != 0:
            raise Exception('One hot array ({}) cannot be devided by corpus size: {}'.format(len(oneHot)))
        for i in range(0, int(number_of_letters)):
            letter_idx = np.argmax(oneHot[i*len(self.corpus):(i*len(self.corpus))+len(self.corpus)])
            text += self.corpus[letter_idx]

        return text
        
    
def train(model, device, train_loader, test_loader, optimizer, epoch, loss_fn, log_interval):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        
        data = Utils.textBatchToOneHot(data, train_loader.getCorpus())
        data = torch.from_numpy(data)
        target = model.targetToIdx(target)
        target = torch.from_numpy(target)
        
        
        
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
        
        if batch_idx % (200000//64) == 0:
            test(model, device, test_loader, loss_fn, epoch)

        if batch_idx % (1000000//64) == 0:
            torch.save(model.state_dict(), "models/1/e"+str(epoch)+"_"+str(batch_idx)+".h5")
            


def test(model, device, test_loader, loss_fn, epoch):
    model.eval()
    with torch.no_grad():
        raw_text, _ = next(iter(test_loader))
        raw_text = list(raw_text)
        whole_text = list(raw_text)
        lastChar = {}
        pairs = {}

        for _ in range(50):
            onehot = np.zeros((64, test_loader.getInputShape()))
            
            for idx, text in enumerate(raw_text):
                onehot[idx] = Utils.textToOneHot(text, test_loader.getCorpus())
                
            
            onehot = torch.from_numpy(onehot).float()
            onehot = onehot.to(device)
            output = model(onehot)

            out_numpy = output.data.cpu().numpy()
            for i in range(len(raw_text)):
                pred_letter = model.oneHotToText(out_numpy[i])
                
                raw_text[i] = (raw_text[i][len(pred_letter):]) + pred_letter
                whole_text[i] += pred_letter
                
                ###################
                if i in lastChar:
                    pair = (lastChar[i], pred_letter)
                    if pair not in pairs:
                        pairs[pair] = 0
                    pairs[pair] += 1
                
                lastChar[i] = pred_letter
                ##################

        ########
        count = 0
        mostUsedPair = None
        for pair in pairs:
            if pairs[pair] > count and pair[0]+""+pair[1] not in model.corpus:
                count = pairs[pair]
                mostUsedPair = pair
        print(mostUsedPair, pairs[mostUsedPair])
        model.add_output(mostUsedPair[0]+""+mostUsedPair[1])    
        ########


        
        test_loader.increaseCounter()

        with open("logs/log_1.txt", "a") as myfile:
            myfile.write("Corpus - ")
            for c in model.corpus:
                myfile.write(c+";")
            myfile.write("\n")
            myfile.write("Test Run - epoch:"+str(epoch)+" counter:"+str(test_loader.counter)+"\n")
            myfile.write("---------------------------\n")
            for idx, text in enumerate(whole_text):
                myfile.write(str(idx)+" - "+str(text)+"\n")

        print(whole_text)
        


if __name__ == '__main__':
    batch_size = 64
    epochs = 10
    lr = 0.01
    momentum = 0.5
    seed = 42
    no_cuda = True
    log_interval = 10

    preTextLength = 40
    postTextLength = 20

    use_cuda = not no_cuda and torch.cuda.is_available()
    torch.manual_seed(seed)
    device = torch.device("cuda" if use_cuda else "cpu")
    print("Running on", device)
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}





    trainTextDataset = TextDataset(preTextLength, postTextLength, train=True)
    testTextDataset = TextDataset(preTextLength, postTextLength, train=False)
    train_loader = TextDataLoader(trainTextDataset, batch_size=batch_size, shuffle=True, **kwargs)
    test_loader  = TextDataLoader(testTextDataset, batch_size=batch_size, shuffle=True, **kwargs)
    corpusLength = len(trainTextDataset.corpus)

    loss_fn = nn.CrossEntropyLoss()
    model = Net(preTextLength, trainTextDataset.corpus).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    for epoch in range(1, epochs + 1):
        train(model, device, train_loader, test_loader, optimizer, epoch, loss_fn, log_interval)
        



