import torch
import torch.nn as nn
import torch.nn.functional as F



if __name__ == "__main__":    
    def my_cross_entropy(x, y):
        log_prob = -1.0 * F.log_softmax(x, 1)
        loss = log_prob.gather(1, y.unsqueeze(1))
        loss = loss.mean()
        return loss


    criterion = nn.CrossEntropyLoss()

    batch_size = 5
    nb_classes = 10
    x = torch.zeros(batch_size, nb_classes, requires_grad=True)
    for i in range(5):
        x[i][i] = 1
    y = torch.LongTensor([0,5,5,5,6])
    
    loss_reference = criterion(x, y)
    loss = my_cross_entropy(x, y)

    print(x)
    print(y)
    print(loss)