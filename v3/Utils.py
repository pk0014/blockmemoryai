import numpy as np
#DATA
def textBatchToOneHot(textbatch, corpus, dtype=np.float32):
    onehot = np.zeros((len(textbatch), len(textbatch[0]) * len(corpus)), dtype=dtype)
    for idx, text in enumerate(textbatch):
        onehot[idx] = textToOneHot(text, corpus, dtype)
    return onehot


def textToOneHot(text, corpus, dtype=np.float32):
    corpus_len = len(corpus)
    onehot = np.zeros((len(text) * corpus_len), dtype=dtype)
    for idx, l in enumerate(text):
        onehot[idx*corpus_len: (idx+1)*corpus_len] = letterToOneHot(l, corpus, dtype)
    
    return onehot

def letterToOneHot(letter, corpus, dtype=np.float32):
    a = np.zeros((len(corpus)), dtype=dtype)
    b = corpus.index(letter)
    a[b] = 1
    return a