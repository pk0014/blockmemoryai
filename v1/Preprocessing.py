import os 
import random

import numpy as np
from torch.utils.data import Dataset

class MyTextDataset(Dataset):
    def __init__(self, preTextLength, postTextLength, train = True):
        self.corpus = []
        self.reviews = []
        self.preTextLength = preTextLength
        self.postTextLength = postTextLength
        self.train = train
        self.split = 0.6

        self.loadFileToMemory()

    def __len__(self):
        l = 0
        for r in self.reviews:
            l += len(r) - (self.preTextLength + self.postTextLength)
        return l

    def __getitem__(self, idx):
        return self.getRandomText(self.preTextLength, self.postTextLength)

    def loadFileToMemory(self, path="dataset/reviews.txt"):

        if(os.path.exists(path) == False):
            raise Exception('File not found : {}'.format(path))

        
        with open(path, 'r') as f:
            reviews_whole = f.read()    
            reviews_whole.replace("  br  ", "")
            reviews_whole.replace("  ", " ")
        reviews_temp = reviews_whole.split('\n')
        
        if self.train:
            reviews_temp = reviews_temp[:int(len(reviews_temp)*self.split)]
        else:
            reviews_temp = reviews_temp[int(len(reviews_temp)*self.split):]

        for r in reviews_temp:
            if len(r) >= self.preTextLength + self.postTextLength:
                self.reviews.append(r)

        self.corpus = set(reviews_whole)
        self.corpus = list(self.corpus)
        self.corpus.remove("\n")

        
    def getRandomText(self, preLength, postLength):
        #TODO: Optimise this
        review = random.choice(self.reviews)
        while not len(review) > preLength+postLength:
            review = random.choice(self.reviews)
        
        startingPoint = random.randint(0, len(review)-(preLength+postLength))
        preText = review[startingPoint:startingPoint + preLength]
        postText = review[startingPoint + preLength: startingPoint + preLength + postLength]
        return self.getTextOnehot(preText), self.getTextToIdx(postText)[0]


    def getManualTest(self):
        review = random.choice(self.reviews)
        while not len(review) > self.preTextLength+self.postTextLength:
            review = random.choice(self.reviews)
        
        startingPoint = random.randint(0, len(review)-(self.preTextLength+self.postTextLength))
        preText = review[startingPoint:startingPoint + self.preTextLength]
        postText = review[startingPoint + self.preTextLength: startingPoint + self.preTextLength + self.postTextLength]
        return self.getTextOnehot(preText), self.getTextToIdx(postText)[0], preText, postText

    def getTextOnehot(self, text, dtype=np.float32):
        onehot = np.zeros((len(text) * len(self.corpus)), dtype=dtype)
        for idx, l in enumerate(text):
            onehot[idx*len(self.corpus): (idx+1)*len(self.corpus)] = self.getLetterOnehot(l, dtype)
        
        return onehot
    
    def getLetterOnehot(self, letter, dtype=np.float32):
        a = np.zeros((len(self.corpus)), dtype=dtype)
        b = self.corpus.index(letter)
        a[b] = 1
        return a

    def getTextToIdx(self, text, dtype=np.int64):
        a = np.zeros((len(text)), dtype=dtype)
        for idx, l in enumerate(text):
            a[idx] = self.corpus.index(l)
        return a

 


if __name__ == '__main__':
    from torch.utils.data import DataLoader

    dataset = MyTextDataset(50, 10)
    dataloader = DataLoader(dataset, batch_size=10, shuffle=True)
    output = next(iter(dataloader))
    print(output[0][0].dtype)