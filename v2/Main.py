from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets, transforms
from TextDataset import TextDataLoader, TextDataset
import numpy as np
import random
from DynamicLinearLayer import DynamicLinearLayer

class Net(nn.Module):
    def __init__(self, preTextLength, postTextLength, corpus):
        super(Net, self).__init__()
        self.corpus = corpus
        self.lin1 = nn.Linear(preTextLength*len(corpus), 100)
        self.relu1 = nn.ReLU()
        self.lin2 = DynamicLinearLayer(100, postTextLength*len(corpus))

    def forward(self, x):
        x = x.view(x.size(0), -1)
        x = self.lin1(x)
        x = self.relu1(x)
        x = self.lin2(x)
        return x


    def expand_output(self, output_features):
        with torch.no_grad():
            self.lin2.expand_output_features(output_features)
    
    def add_output(self, text):
        self.corpus.append(text)
        self.expand_output(len(self.corpus))

        
    
def train(model, device, train_loader, test_loader, optimizer, epoch, loss_fn, log_interval):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        target = target[:,0]
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
        
        if batch_idx % (100000//64) == 0:
            test(model, device, test_loader, loss_fn)

        if batch_idx == 100:
            model.add_output("the")
            


def test(model, device, test_loader, loss_fn):
    model.eval()
    with torch.no_grad():
        _, raw_text = next(iter(test_loader))
        raw_text = list(raw_text)
        whole_text = list(raw_text)
        for i in range(50):
            onehot = np.zeros((64, test_loader.getInputShape()))
            for idx, text in enumerate(raw_text):
                onehot[idx] = test_loader.textToOneHot(text)
            
            onehot = torch.from_numpy(onehot).float()
            onehot = onehot.to(device)
            output = model(onehot)

            out_numpy = output.data.cpu().numpy()
            for i in range(len(raw_text)):
                pred_letter = test_loader.oneHotToText(out_numpy[i])
                raw_text[i] = (raw_text[i][1:]) + pred_letter
                whole_text[i] += pred_letter

        
        test_loader.increaseCounter()

        with open("logs/log_1.txt", "a") as myfile:
            myfile.write("Test Run - "+str(test_loader.counter)+"\n")
            myfile.write("---------------------------\n")
            for idx, text in enumerate(whole_text):
                myfile.write(str(idx)+" - "+str(text)+"\n")

        print(whole_text)
        


if __name__ == '__main__':
    batch_size = 64
    epochs = 1
    lr = 0.01
    momentum = 0.5
    seed = 42
    no_cuda = True
    log_interval = 10

    preTextLength = 40
    postTextLength = 1


    use_cuda = not no_cuda and torch.cuda.is_available()
    torch.manual_seed(seed)
    device = torch.device("cuda" if use_cuda else "cpu")
    print("Running on", device)
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}


    trainTextDataset = TextDataset(preTextLength, postTextLength, train=True)
    testTextDataset = TextDataset(preTextLength, postTextLength, train=False)
    train_loader = TextDataLoader(trainTextDataset, batch_size=batch_size, shuffle=True, **kwargs)
    test_loader  = TextDataLoader(testTextDataset, batch_size=batch_size, shuffle=True, **kwargs)
    corpusLength = len(trainTextDataset.corpus)

    loss_fn = nn.CrossEntropyLoss()
    model = Net(preTextLength, postTextLength, corpusLength).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    for epoch in range(1, epochs + 1):
        train(model, device, train_loader, test_loader, optimizer, epoch, loss_fn, log_interval)
        



